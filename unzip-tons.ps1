$ZipFilesPath = Read-Host -Prompt 'Input the Location of Zip Files'
$UnzipPath = Read-Host -Prompt 'Input the Destination of extraction'
$TextFile = Read-Host -Prompt 'Input the Full path of the Seq File'
 
$Shell = New-Object -com Shell.Application
$Location = $Shell.NameSpace($UnzipPath)
 
$ZipFiles = Get-Childitem $ZipFilesPath -Recurse -Include *.ZIP
$content = Get-Content $TextFile
$progress = 1
$Index = 1
foreach($line in $content) {
  $ZipFile = Get-ChildItem -Path $ZipFilesPath -Recurse -Include *$line*
  $NewLocation = "$($UnzipPath)\doc\$Index"
  New-Item $NewLocation -type Directory
   
  Write-Progress -Activity "Unzipping to $($UnzipPath)" -PercentComplete (($progress / ($ZipFiles.Count + 1)) * 100) -CurrentOperation $ZipFile.FullName -Status "File $($Progress) of $($ZipFiles.Count)"
   Copy-Item $ZipFile.fullname $NewLocation
   $NewZipFile = get-childitem $NewLocation *.zip
   $NewLocation = $shell.namespace($NewLocation)
    $ZipFolder = $shell.namespace($NewZipFile.fullname)
    $NewLocation.copyhere($ZipFolder.items())
   $ZipFolder = $Shell.NameSpace($ZipFile.fullname)
   $Location.Copyhere($ZipFolder.items(), 1040)   
   get-childitem "$($UnzipPath)\doc\$Index\" -exclude doc -recurse | ?{ $_.psiscontainer } | remove-item -Recurse
   $Index = $Index + 1
   $progress++
   Remove-Item "$($UnzipPath)\doc\*.*"
   
}